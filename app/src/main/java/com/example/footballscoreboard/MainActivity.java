package com.example.footballscoreboard;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
public class MainActivity extends AppCompatActivity {

    private Integer pointsOfTeamZenit = 0;
    private Integer pointsOfTeamCSKA = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        outputOnDisplay();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("TeamZenit", pointsOfTeamZenit);
        outState.putInt("TeamCSKA", pointsOfTeamCSKA);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey("TeamZenit")) {
            pointsOfTeamZenit = savedInstanceState.getInt("TeamZenit");
        }

        if (savedInstanceState.containsKey("TeamCSKA")) {
            pointsOfTeamCSKA = savedInstanceState.getInt("TeamCSKA");
        }

        outputOnDisplay();
    }

    public void onClickBtnAddPoints1(View view) {
        pointsOfTeamZenit++;

        outputOnDisplay();
    }
    public void onClickBtnAddPoints2(View view) {

        pointsOfTeamCSKA++;
        outputOnDisplay();
    }


    @SuppressLint("SetTextI18n")
    private void outputOnDisplay() {

        TextView rusTeam = findViewById(R.id.first_team);
        rusTeam.setText(pointsOfTeamZenit.toString());

        TextView usaTeam = findViewById(R.id.second_team);
        usaTeam.setText(pointsOfTeamCSKA.toString());
    }
}